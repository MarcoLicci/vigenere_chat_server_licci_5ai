package com.marcolicci.vigenere.chat.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Main {
    private static ServerSocket server;
    private static int port = 9876;

    private static ArrayList<PrintWriter> socketWriters = new ArrayList<>();

    public static void main(String args[]) throws IOException {
        server = new ServerSocket(port);

        while(true) {
            Socket socket = server.accept();
            socketWriters.add(new PrintWriter(socket.getOutputStream(), true));
            BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            new Thread(() -> {
                try {
                    String line;
                    while ((line = socketReader.readLine()) != null) {
                        for (PrintWriter sw : socketWriters) {
                            sw.println(line);
                        }
                    }
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

}